package iclient

import (
	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2/messages"
)

type IClient interface {
	Log(message string)
	ErrorCallback(messages.WSMessage)

	BeforeStartContainerCallback(messages.WSMessage)
	AfterStartContainerCallback(messages.WSMessage)
	BeforeCreateContainerCallback(messages.WSMessage)
	AfterCreateContainerCallback(messages.WSMessage)
	BeforeExecContainerCallback(messages.WSMessage)
	AfterExecContainerCallback(messages.WSMessage)
	BeforeRemoveContainerCallback(messages.WSMessage)
	AfterRemoveContainerCallback(messages.WSMessage)
	BeforeGetContainersCallback(messages.WSMessage)
	AfterGetContainersCallback(messages.WSMessage)
	BeforeGetContainerWithSameNameCallback(messages.WSMessage)
	AfterGetContainerWithSameNameCallback(messages.WSMessage)
	BeforeGetContainerLogCallback(messages.WSMessage)
	AfterGetContainerLogCallback(messages.WSMessage)
	BeforePullImageCallback(messages.WSMessage)
	AfterPullImageCallback(messages.WSMessage)
	BeforeGetConfigCallback(messages.WSMessage)
	AfterGetConfigCallback(messages.WSMessage)
	BeforePingCallback(messages.WSMessage)
	AfterPingCallback(messages.WSMessage)
	BeforeTestCallback(messages.WSMessage)
	AfterTestCallback(messages.WSMessage)
	BeforeTestErrorCallback(messages.WSMessage)
	AfterTestErrorCallback(messages.WSMessage)
}
