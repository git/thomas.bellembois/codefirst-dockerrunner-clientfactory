package messagesender

import (
	"context"
	"encoding/base64"
	"encoding/json"

	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2/actions"
	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2/credentials"
	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2/messages"
	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2/models"
	"github.com/docker/docker/api/types"
	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

type IMessageSender interface {
	GetContext() context.Context
	GetCancelFunc() context.CancelFunc
	GetDialOptions() *websocket.DialOptions
}

type AbstractMessageSender struct {
	IMessageSender

	WSConnection *websocket.Conn
}

func (ms *AbstractMessageSender) GetRunningContainers() {
	if err := wsjson.Write(ms.GetContext(), ms.WSConnection,
		messages.WSMessage{
			Action: actions.GetContainers,
		},
	); err != nil {
		panic(err)
	}
}

func (ms *AbstractMessageSender) GetContainerLog(containerID string) {
	message := messages.WSMessage{
		Action: actions.GetContainerLog,
		Container: models.CodeFirstContainer{
			ID: containerID,
		}}

	if err := wsjson.Write(ms.GetContext(), ms.WSConnection, message); err != nil {
		panic(err)
	}
}

func (ms *AbstractMessageSender) RemoveContainer(containerID string) {
	message := messages.WSMessage{
		Action: actions.RemoveContainer,
		Container: models.CodeFirstContainer{
			ID: containerID,
		}}

	if err := wsjson.Write(ms.GetContext(), ms.WSConnection, message); err != nil {
		panic(err)
	}
}

func (ms *AbstractMessageSender) ExecContainer(containerName, user string, cmd, env []string) {
	message := messages.WSMessage{
		Action: actions.ExecContainer,
		Container: models.CodeFirstContainer{
			Name: containerName,
		},
		Exec: types.ExecConfig{
			User: user,
			Cmd:  cmd,
			Env:  env,
		},
	}

	if err := wsjson.Write(ms.GetContext(), ms.WSConnection, message); err != nil {
		panic(err)
	}
}

func (ms *AbstractMessageSender) StartContainer(imageName, containerName, username, password string, private, overwrite bool, env []string) {
	userCredentials := credentials.RegistryCredentials{
		Username: username,
		Password: password,
	}

	marshalUserCredentials, err := json.Marshal(userCredentials)
	if err != nil {
		panic(err)
	}

	message := messages.WSMessage{
		Action: actions.StartContainer,
		Container: models.CodeFirstContainer{
			ImageURL:          imageName,
			Name:              containerName,
			Base64Credentials: base64.StdEncoding.EncodeToString(marshalUserCredentials),
			Env:               env,
			Private:           private,
			Overwrite:         overwrite,
		}}

	if err := wsjson.Write(ms.GetContext(), ms.WSConnection, message); err != nil {
		panic(err)
	}
}

func (ms *AbstractMessageSender) Test() {
	if err := wsjson.Write(ms.GetContext(), ms.WSConnection,
		messages.WSMessage{
			Action:  actions.Test,
			Message: "this is a test message",
		},
	); err != nil {
		panic(err)
	}
}

func (ms *AbstractMessageSender) TestError() {
	if err := wsjson.Write(ms.GetContext(), ms.WSConnection,
		messages.WSMessage{
			Action:  actions.TestError,
			Message: "this is a test error message",
		},
	); err != nil {
		panic(err)
	}
}
