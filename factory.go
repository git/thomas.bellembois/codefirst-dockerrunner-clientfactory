package clientfactory

import (
	"fmt"
	"time"

	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2/actions"
	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2/callbacks"
	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2/messages"

	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-clientfactory/v2/iclient"
	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-clientfactory/v2/messagesender"

	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

var (
	gotconfig chan (bool)
)

type AbstractClient struct {
	iclient.IClient

	MessageSender *messagesender.AbstractMessageSender

	WSScheme, WSHost, WSPath string
	MaxAllowedContainers     int

	NbRunningContainers int
}

func (c *AbstractClient) Run() {
	gotconfig = make(chan bool)

	// Listen to server websocker messages.
	c.ReadServerEvents()

	// Retrieving configuration.
	c.GetConfiguration()
	<-gotconfig

	// Pinging server every x seconds to keep TCP connection alive.
	c.PingServer()
}

func (c *AbstractClient) ReadServerEvents() {
	c.Log("Waiting for messages from server.")

	go func() {
		for {
			var (
				message messages.WSMessage
				err     error
			)

			if err = wsjson.Read(c.MessageSender.GetContext(), c.MessageSender.WSConnection, &message); err != nil {
				panic(err)
			}

			if message.Error != nil {
				c.ErrorCallback(message)
			} else {

				switch message.Action {
				case callbacks.Log:
					c.Log(message.Message)

				case callbacks.BeforeStartContainerCallback:
					c.Log("Message BeforeStartContainerCallback")
					c.BeforeStartContainerCallback(message)

				case callbacks.AfterStartContainerCallback:
					c.Log("Message AfterStartContainerCallback")
					c.AfterStartContainerCallback(message)

				case callbacks.BeforeExecContainerCallback:
					c.Log("Message BeforeExecContainerCallback")
					c.BeforeExecContainerCallback(message)

				case callbacks.AfterExecContainerCallback:
					c.Log("Message AfterExecContainerCallback")
					c.AfterExecContainerCallback(message)

				case callbacks.BeforeCreateContainerCallback:
					c.Log("Message BeforeCreateContainerCallback")
					c.BeforeCreateContainerCallback(message)

				case callbacks.AfterCreateContainerCallback:
					c.Log("Message AfterCreateContainerCallback")
					c.AfterCreateContainerCallback(message)

				case callbacks.BeforeRemoveContainerCallback:
					c.Log("Message BeforeRemoveContainerCallback")
					c.BeforeRemoveContainerCallback(message)

				case callbacks.AfterRemoveContainerCallback:
					c.Log("Message AfterRemoveContainerCallback")
					c.AfterRemoveContainerCallback(message)

				case callbacks.BeforeGetContainersCallback:
					c.Log("Message BeforeGetContainersCallback")
					c.BeforeGetContainersCallback(message)

				case callbacks.AfterGetContainersCallback:
					c.Log("Message AfterGetContainersCallback")
					c.NbRunningContainers = len(message.Containers)

					c.AfterGetContainersCallback(message)

				case callbacks.BeforeGetContainerWithSameNameCallback:
					c.Log("Message BeforeGetContainerWithSameNameCallback")
					c.BeforeGetContainerWithSameNameCallback(message)

				case callbacks.AfterGetContainerWithSameNameCallback:
					c.Log("Message AfterGetContainerWithSameNameCallback")
					c.AfterGetContainerWithSameNameCallback(message)

				case callbacks.BeforeGetContainerLogCallback:
					c.Log("Message BeforeGetContainerLogCallback")
					c.BeforeGetContainerLogCallback(message)

				case callbacks.AfterGetContainerLogCallback:
					c.Log("Message AfterGetContainerLogCallback")
					c.AfterGetContainerLogCallback(message)

				case callbacks.BeforePullImageCallback:
					c.Log("Message BeforePullImageCallback")
					c.BeforePullImageCallback(message)

				case callbacks.AfterPullImageCallback:
					c.Log("Message AfterPullImageCallback")
					c.AfterPullImageCallback(message)

				case callbacks.BeforeGetConfigCallback:
					c.Log("Message BeforeGetConfigCallback")
					c.BeforeGetConfigCallback(message)

				case callbacks.AfterGetConfigCallback:
					c.Log("Message AfterGetConfigCallback")
					c.MaxAllowedContainers = message.Config.MaxAllowedContainers
					gotconfig <- true

					c.AfterGetConfigCallback(message)

				case callbacks.BeforePingCallback:
					c.BeforePingCallback(message)

				case callbacks.AfterPingCallback:
					c.AfterPingCallback(message)

				case callbacks.BeforeTestCallback:
					c.BeforeTestCallback(message)

				case callbacks.AfterTestCallback:
					c.AfterTestCallback(message)

				case callbacks.BeforeTestErrorCallback:
					c.BeforeTestErrorCallback(message)

				case callbacks.AfterTestErrorCallback:
					c.AfterTestErrorCallback(message)

				default:
					c.Log(fmt.Sprintf("unexpected message received: %+v", message))
				}
			}

		}
	}()
}

func (c *AbstractClient) ConnectServer() {
	var (
		err error
	)

	wsURL := fmt.Sprintf("%s://%s%sws/", c.WSScheme, c.WSHost, c.WSPath)

	c.Log("Connecting to websocket at " + wsURL)

	if c.MessageSender.WSConnection, _, err = websocket.Dial(c.MessageSender.GetContext(), wsURL, c.MessageSender.GetDialOptions()); err != nil {
		panic(err)
	}
}

func (c *AbstractClient) GetConfiguration() {
	c.Log("Getting configuration.")

	if err := wsjson.Write(c.MessageSender.GetContext(), c.MessageSender.WSConnection,
		messages.WSMessage{
			Action: actions.GetConfig,
		},
	); err != nil {
		panic(err)
	}
}

func (c *AbstractClient) PingServer() {
	c.Log("Pinging server.")

	go func() {
		message := messages.WSMessage{
			Action: actions.Ping,
		}

		for {
			err := wsjson.Write(c.MessageSender.GetContext(), c.MessageSender.WSConnection, message)
			if err != nil {
				panic(err)
			}

			time.Sleep(30 * time.Second)
		}
	}()
}
