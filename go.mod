module codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-clientfactory/v2

go 1.19

require (
	codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2 v2.0.0-20220905134948-382dd0bb6a40
	github.com/docker/docker v20.10.17+incompatible
	nhooyr.io/websocket v1.8.7
)

require (
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.5.0 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/opencontainers/image-spec v1.0.2 // indirect
)

// replace codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2 => /home/thbellem/workspace/workspace_go/src/codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common
